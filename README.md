# XpertSelect / PSR Tools

[gitlab.com/xpertselect/psr-tools](https://gitlab.com/xpertselect/psr-tools)

XpertSelect package providing PSR Tools for interacting with various PSR-compliant implementations.

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of [`xpertselect/psr-tools`](https://packagist.org/packages/xpertselect/psr-tools) is done via [Composer](https://getcomposer.org).

```shell
composer require xpertselect/psr-tools
```
