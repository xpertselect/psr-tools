<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\StoppableEventInterface;
use Tests\TestCase;
use XpertSelect\PsrTools\StoppableEvent;

/**
 * @internal
 */
final class StoppableEventTest extends TestCase
{
    private StoppableEvent $sut;

    public function setUp(): void
    {
        $this->sut = new class () extends StoppableEvent {
        };
    }

    public function testClassImplementsThePsrInterface(): void
    {
        Assert::assertInstanceOf(StoppableEventInterface::class, $this->sut);
    }

    public function testPropagationIsNotStoppedByDefault(): void
    {
        Assert::assertFalse($this->sut->isPropagationStopped());
    }

    public function testPropagationCanBeStopped(): void
    {
        Assert::assertFalse($this->sut->isPropagationStopped());

        $this->sut->stopPropagation();

        Assert::assertTrue($this->sut->isPropagationStopped());
    }
}
