<?php

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use PHPUnit\Framework\Assert;
use Psr\Http\Message\StreamInterface;
use Tests\TestCase;
use XpertSelect\PsrTools\Payload;

/**
 * @internal
 */
class PayloadTest extends TestCase
{
    /**
     * The Payload instance.
     */
    private Payload $payload;

    /**
     * This method is called before each test.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->payload = new Payload();
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown(): void
    {
        unset($this->payload);
    }

    public function testPayloadHasNoValuesByDefault(): void
    {
        Assert::assertFalse($this->payload->hasValues());
    }

    public function testPayloadValuesAreSetFromTheConstructor(): void
    {
        $payload = new Payload(['foo' => 'bar']);

        Assert::assertTrue($payload->hasValues());
        Assert::assertCount(1, $payload->valuesAsArray());
    }

    public function testHasFilesWithoutFiles(): void
    {
        $this->assertFalse($this->payload->hasFiles());
    }

    public function testHasFilesWithFile(): void
    {
        $this->payload->addFile('test', __FILE__);
        $this->assertTrue($this->payload->hasFiles());
    }

    public function testAddValues()
    {
        $values = [
            'value1' => 'test1',
            'value2' => [
                'subvalue1' => 'subtest1',
                'subvalue2' => 'subtest2',
            ],
            'value3' => 'test3',
        ];

        $this->payload->addValues($values);
        $this->assertSame($values, $this->payload->valuesAsArray());

        $newValues = [
            'value1' => 'newtest1',
            'value3' => [
                'subvalue1' => 'subtest1',
            ],
        ];

        $this->payload->addValues($newValues);

        $comparableResult = $this->payload->valuesAsArray();

        $this->assertSame($newValues['value1'], $comparableResult['value1']);
        $this->assertSame($newValues['value3'], $comparableResult['value3']);
        $this->assertSame($values['value2'], $comparableResult['value2']);
    }

    public function testAddValue(): void
    {
        $name     = 'key';
        $contents = 'value';

        $this->payload->addValue($name, $contents);

        $expected = [$name => $contents];
        $actual   = $this->payload->valuesAsArray();

        $this->assertEquals($expected, $actual, 'The value was not added correctly to the payload object.');
    }

    public function testAddValueWithArrayContents(): void
    {
        $name     = 'key';
        $contents = ['subKey' => 'subValue'];

        $this->payload->addValue($name, $contents);

        $expected = [$name => $contents];
        $actual   = $this->payload->valuesAsArray();

        $this->assertEquals($expected, $actual, 'The array value was not added correctly to the payload object.');
    }

    public function testAddValueOverwritesOldValue(): void
    {
        $name           = 'key';
        $contentsFirst  = 'firstValue';
        $contentsSecond = 'secondValue';

        $this->payload->addValue($name, $contentsFirst);
        $this->payload->addValue($name, $contentsSecond);

        $expected = [$name => $contentsSecond];
        $actual   = $this->payload->valuesAsArray();

        $this->assertEquals($expected, $actual, 'The old value was not overwritten correctly in the payload object.');
    }

    public function testAddValueRemovesFileIfFileWithNameExists(): void
    {
        $this->payload->addFile('test', __FILE__);
        $this->assertTrue($this->payload->hasFiles());
        $this->payload->addValue('test', 'value');
        $this->assertFalse($this->payload->hasFiles());
    }

    public function testAddFileRemovesValueIfValueWithNameExists(): void
    {
        $this->payload->addValue('test', 'value');
        $this->payload->addFile('test', __FILE__);
        $this->assertEmpty($this->payload->valuesAsArray());
    }

    public function testValuesAsArray(): void
    {
        $values = [
            'name'   => 'John Doe',
            'email'  => 'john@example.com',
            'gender' => 'M',
        ];

        // Add values to the payload
        $this->payload->addValues($values);

        // Assert that returned array matches the one we added
        $this->assertSame($values, $this->payload->valuesAsArray());
    }

    public function testSetBoundary(): void
    {
        $boundary = '-----011000010111000001101001';
        $this->payload->setBoundary($boundary);

        $getBoundary = $this->payload->getBoundary();

        $this->assertEquals($boundary, $getBoundary, "Boundary should be equals to {$boundary}");
    }

    public function testGetBoundary(): void
    {
        // Define a boundary
        $boundary = '-----011000010111000001101001';
        // Set the boundary
        $this->payload->setBoundary($boundary);
        // Assert that the set boundary matches what is returned
        $this->assertEquals($boundary, $this->payload->getBoundary());
    }

    public function testGetStream(): void
    {
        // Single dimensional
        $this->payload->addValue('key', 'value');

        // Multidimensional
        $this->payload->addValue('multi', ['level1' => ['level2' => 'value']]);

        $this->payload->addFile('fileKey', __FILE__, 'test_file.txt');

        // When we call getStream method
        $stream = $this->payload->getStream();

        // Then we expect the return value to be an instance of StreamInterface
        $this->assertInstanceOf(StreamInterface::class, $stream);

        // We will also check that the stream contains the data we added to the payload
        $streamContents = $stream->__toString();
        $this->assertStringContainsString('key="value"', $streamContents);
        // assertion for multidimensional array
        $this->assertStringContainsString('multi[level1][level2]="value"', $streamContents);
        $this->assertStringContainsString('filename="test_file.txt"', $streamContents);
    }
}
