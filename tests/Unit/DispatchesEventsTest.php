<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Psr\EventDispatcher\EventDispatcherInterface;
use stdClass;
use Tests\TestCase;
use XpertSelect\PsrTools\DispatchesEvents;

/**
 * @internal
 */
final class DispatchesEventsTest extends TestCase
{
    public function testEventIsDispatchedWhenDispatcherIsPresent(): void
    {
        $event      = new stdClass();
        $dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)
            ->onlyMethods(['dispatch'])
            ->getMock();

        $dispatcher->expects($this->once())->method('dispatch')->with($event);

        $subject = new class ($dispatcher) {
            use DispatchesEvents;

            public function __construct(EventDispatcherInterface $dispatcher)
            {
                $this->eventDispatcher = $dispatcher;
            }
        };

        $subject->dispatchEvent($event);
    }
}
