<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use GuzzleHttp\Psr7\HttpFactory;
use GuzzleHttp\Psr7\Request;
use Mockery as M;
use PHPUnit\Framework\Assert;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use Tests\TestCase;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\HttpRequestService;
use XpertSelect\PsrTools\Payload;
use XpertSelect\PsrTools\PsrResponse;

/**
 * @internal
 */
final class HttpRequestServiceTest extends TestCase
{
    public static function endpointDataset(): array
    {
        return [
            ['https://example.com', 'https://example.com'],
            ['https://example.com/', 'https://example.com'],
            ['https://example.com:8080', 'https://example.com:8080'],
            ['https://example.com:8080/', 'https://example.com:8080'],
            ['https://example.com/foo', 'https://example.com/foo'],
            ['https://example.com/foo/', 'https://example.com/foo'],
            ['https://example.com:8080/foo', 'https://example.com:8080/foo'],
            ['https://example.com:8080/foo/', 'https://example.com:8080/foo'],
        ];
    }

    public function testHasApiKey(): void
    {
        $httpRequestService = $this->getHttpRequestServiceInstance(
            '',
            M::mock(ClientInterface::class),
            M::mock(RequestFactoryInterface::class),
            M::mock(StreamFactoryInterface::class)
        );

        Assert::assertFalse($httpRequestService->hasApiKey());

        $httpRequestService->setApiKey('foo');

        Assert::assertTrue($httpRequestService->hasApiKey());
    }

    /**
     * @dataProvider endpointDataset
     */
    public function testTrailingSlashIsRemovedFromEndpoint(string $endpoint, string $validated): void
    {
        $httpRequestService = $this->getHttpRequestServiceInstance(
            $endpoint,
            M::mock(ClientInterface::class),
            M::mock(RequestFactoryInterface::class),
            M::mock(StreamFactoryInterface::class)
        );

        Assert::assertEquals($validated, $httpRequestService->getEndpoint());
    }

    public function testGetCreatesPsrGetRequest(): void
    {
        try {
            $httpRequestService = $this->getHttpRequestServiceInstance(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                M::mock(StreamFactoryInterface::class)
            );

            Assert::assertEmpty($this->guzzleHistory);

            $httpRequestService->get('bar', ['lorem' => 'ipsum']);

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals('GET', strtoupper($request->getMethod()));
                Assert::assertEquals('/bar', $request->getUri()->getPath());
                Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());

                Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
                Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testPostWithFilesExecutesPostStreamRequest(): void
    {
        $payload = new Payload();
        $payload->addFile('file', __FILE__, 'filename.txt');
        $boundary           = $payload->getBoundary();
        $httpRequestService = $this->getHttpRequestServiceInstance(
            'https://example.com',
            $this->createPsrClient(),
            new HttpFactory(),
            new HttpFactory(),
        );

        Assert::assertEmpty($this->guzzleHistory);

        $httpRequestService->post('bar', $payload, ['lorem' => 'ipsum']);

        Assert::assertNotEmpty($this->guzzleHistory);
        foreach ($this->guzzleHistory as $entry) {
            Assert::assertArrayHasKey('request', $entry);

            /** @var Request $request */
            $request = $entry['request'];

            Assert::assertEquals('POST', strtoupper($request->getMethod()));
            Assert::assertEquals('/bar', $request->getUri()->getPath());
            Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());

            Assert::assertEquals(['multipart/form-data; boundary="' . $boundary . '"'], $request->getHeader('Content-Type'));
            Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
            Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
        }
    }

    public function testPostWithoutFilesExecutesPostJsonRequest(): void
    {
        $payload = new Payload();
        $payload->addValue('foo', 'abc');
        $httpRequestService = $this->getHttpRequestServiceInstance(
            'https://example.com',
            $this->createPsrClient(),
            new HttpFactory(),
            new HttpFactory(),
        );

        Assert::assertEmpty($this->guzzleHistory);

        $httpRequestService->post('bar', $payload, ['lorem' => 'ipsum']);

        Assert::assertNotEmpty($this->guzzleHistory);
        foreach ($this->guzzleHistory as $entry) {
            Assert::assertArrayHasKey('request', $entry);

            /** @var Request $request */
            $request = $entry['request'];

            Assert::assertEquals('POST', strtoupper($request->getMethod()));
            Assert::assertEquals('/bar', $request->getUri()->getPath());
            Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());

            Assert::assertEquals(['application/json'], $request->getHeader('Content-Type'));
            Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
            Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
        }
    }

    public function testPutWithFilesExecutesPutStreamRequest(): void
    {
        $payload = new Payload();
        $payload->addFile('file', __FILE__, 'filename.txt');
        $boundary           = $payload->getBoundary();
        $httpRequestService = $this->getHttpRequestServiceInstance(
            'https://example.com',
            $this->createPsrClient(),
            new HttpFactory(),
            new HttpFactory(),
        );

        Assert::assertEmpty($this->guzzleHistory);

        $httpRequestService->put('bar', $payload, ['lorem' => 'ipsum']);

        Assert::assertNotEmpty($this->guzzleHistory);
        foreach ($this->guzzleHistory as $entry) {
            Assert::assertArrayHasKey('request', $entry);

            /** @var Request $request */
            $request = $entry['request'];

            Assert::assertEquals('PUT', strtoupper($request->getMethod()));
            Assert::assertEquals('/bar', $request->getUri()->getPath());
            Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());

            Assert::assertEquals(['multipart/form-data; boundary="' . $boundary . '"'], $request->getHeader('Content-Type'));
            Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
            Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
        }
    }

    public function testPutWithoutFilesExecutesPutJsonRequest(): void
    {
        $payload = new Payload();
        $payload->addValue('foo', 'abc');
        $httpRequestService = $this->getHttpRequestServiceInstance(
            'https://example.com',
            $this->createPsrClient(),
            new HttpFactory(),
            new HttpFactory(),
        );

        Assert::assertEmpty($this->guzzleHistory);

        $httpRequestService->put('bar', $payload, ['lorem' => 'ipsum']);

        Assert::assertNotEmpty($this->guzzleHistory);
        foreach ($this->guzzleHistory as $entry) {
            Assert::assertArrayHasKey('request', $entry);

            /** @var Request $request */
            $request = $entry['request'];

            Assert::assertEquals('PUT', strtoupper($request->getMethod()));
            Assert::assertEquals('/bar', $request->getUri()->getPath());
            Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());

            Assert::assertEquals(['application/json'], $request->getHeader('Content-Type'));
            Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
            Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
        }
    }

    public function testPatchWithFilesExecutesPatchStreamRequest(): void
    {
        $payload = new Payload();
        $payload->addFile('file', __FILE__, 'filename.txt');
        $boundary           = $payload->getBoundary();
        $httpRequestService = $this->getHttpRequestServiceInstance(
            'https://example.com',
            $this->createPsrClient(),
            new HttpFactory(),
            new HttpFactory(),
        );

        Assert::assertEmpty($this->guzzleHistory);

        $httpRequestService->patch('bar', $payload, ['lorem' => 'ipsum']);

        Assert::assertNotEmpty($this->guzzleHistory);
        foreach ($this->guzzleHistory as $entry) {
            Assert::assertArrayHasKey('request', $entry);

            /** @var Request $request */
            $request = $entry['request'];

            Assert::assertEquals('PATCH', strtoupper($request->getMethod()));
            Assert::assertEquals('/bar', $request->getUri()->getPath());
            Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());

            Assert::assertEquals(['multipart/form-data; boundary="' . $boundary . '"'], $request->getHeader('Content-Type'));
            Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
            Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
        }
    }

    public function testPatchWithoutFilesExecutesPatchJsonRequest(): void
    {
        $payload = new Payload();
        $payload->addValue('foo', 'abc');
        $httpRequestService = $this->getHttpRequestServiceInstance(
            'https://example.com',
            $this->createPsrClient(),
            new HttpFactory(),
            new HttpFactory(),
        );

        Assert::assertEmpty($this->guzzleHistory);

        $httpRequestService->patch('bar', $payload, ['lorem' => 'ipsum']);

        Assert::assertNotEmpty($this->guzzleHistory);
        foreach ($this->guzzleHistory as $entry) {
            Assert::assertArrayHasKey('request', $entry);

            /** @var Request $request */
            $request = $entry['request'];

            Assert::assertEquals('PATCH', strtoupper($request->getMethod()));
            Assert::assertEquals('/bar', $request->getUri()->getPath());
            Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());

            Assert::assertEquals(['application/json'], $request->getHeader('Content-Type'));
            Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
            Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
        }
    }

    public function testPostJsonCreatesPsrPostRequest(): void
    {
        try {
            $httpRequestService = $this->getHttpRequestServiceInstance(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            Assert::assertEmpty($this->guzzleHistory);

            $httpRequestService->postJson('bar', ['bar' => 'baz'], ['lorem' => 'ipsum']);

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals('POST', strtoupper($request->getMethod()));
                Assert::assertEquals('/bar', $request->getUri()->getPath());
                Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());
                Assert::assertEquals('{"bar":"baz"}', $request->getBody()->getContents());

                Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
                Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testPutJsonCreatesPsrPutRequest(): void
    {
        try {
            $httpRequestService = $this->getHttpRequestServiceInstance(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            Assert::assertEmpty($this->guzzleHistory);

            $httpRequestService->putJson('bar', ['lorem' => 'ipsum'], ['bar' => 'baz']);

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals('PUT', strtoupper($request->getMethod()));
                Assert::assertEquals('/bar', $request->getUri()->getPath());
                Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());
                Assert::assertEquals('{"bar":"baz"}', $request->getBody()->getContents());

                Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
                Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testPatchJsonCreatesPsrPatchRequest(): void
    {
        try {
            $httpRequestService = $this->getHttpRequestServiceInstance(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            Assert::assertEmpty($this->guzzleHistory);

            $httpRequestService->patchJson('bar', ['lorem' => 'ipsum'], ['bar' => 'baz']);

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals('PATCH', strtoupper($request->getMethod()));
                Assert::assertEquals('/bar', $request->getUri()->getPath());
                Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());
                Assert::assertEquals('{"bar":"baz"}', $request->getBody()->getContents());

                Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
                Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testDeleteCreatesPsrDeleteRequest(): void
    {
        try {
            $httpRequestService = $this->getHttpRequestServiceInstance(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            Assert::assertEmpty($this->guzzleHistory);

            $httpRequestService->delete('bar', ['lorem' => 'ipsum']);

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals('DELETE', strtoupper($request->getMethod()));
                Assert::assertEquals('/bar', $request->getUri()->getPath());
                Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());

                Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
                Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testBooleanGetParametersAreBuiltCorrectly(): void
    {
        try {
            $httpRequestService = $this->getHttpRequestServiceInstance(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            Assert::assertEmpty($this->guzzleHistory);

            $httpRequestService->get('bar', ['lorem' => true]);

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals('GET', strtoupper($request->getMethod()));
                Assert::assertEquals('/bar', $request->getUri()->getPath());
                Assert::assertEquals('lorem=true', $request->getUri()->getQuery());

                Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
                Assert::assertEquals(['test-user-agent'], $request->getHeader('User-Agent'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testApiKeyIsIncludedAsAuthorizationHeaderWithJsonRequest(): void
    {
        try {
            $httpRequestService = $this->getHttpRequestServiceInstance(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                M::mock(StreamFactoryInterface::class)
            );

            Assert::assertEmpty($this->guzzleHistory);
            Assert::assertFalse($httpRequestService->hasApiKey());

            $httpRequestService->setApiKey('foo');
            $httpRequestService->get('/');

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals(['foo'], $request->getHeader('Authorization'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testApiKeyIsIncludedAsAuthorizationHeaderWithStreamRequest(): void
    {
        try {
            $httpRequestService = $this->getHttpRequestServiceInstance(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                M::mock(StreamFactoryInterface::class)
            );

            Assert::assertEmpty($this->guzzleHistory);
            Assert::assertFalse($httpRequestService->hasApiKey());

            $httpRequestService->setApiKey('foo');
            $httpRequestService->postStream('/', $this->createStream('body'), '123123123', []);

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals(['foo'], $request->getHeader('Authorization'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testContentTypeHeaderIsSetWhenPostingJsonData(): void
    {
        try {
            $httpRequestService = $this->getHttpRequestServiceInstance(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            Assert::assertEmpty($this->guzzleHistory);
            $httpRequestService->postJson('/', jsonData: ['foo' => 'bar']);

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals(['application/json'], $request->getHeader('Content-type'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testNoContentTypeHeaderIsSetWhenPostingWithStream(): void
    {
        try {
            $httpRequestService = $this->getHttpRequestServiceInstance(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            Assert::assertEmpty($this->guzzleHistory);
            $httpRequestService->postStream('/', $this->createStream('body'), 'abcabc', []);

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals(['multipart/form-data; boundary="abcabc"'], $request->getHeader('Content-type'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    private function getHttpRequestServiceInstance(string $endpoint, ClientInterface $httpClient,
                                                   RequestFactoryInterface $requestFactory,
                                                   StreamFactoryInterface $streamFactory): HttpRequestService
    {
        return new class ($endpoint, $httpClient, $requestFactory, $streamFactory) extends HttpRequestService {
            /**
             * {@inheritdoc}
             */
            public function createUserAgent(): string
            {
                return 'test-user-agent';
            }

            /**
             * {@inheritdoc}
             */
            public function getPsrResponse(ResponseInterface $response): PsrResponse
            {
                return new class ($response) extends PsrResponse {
                    /**
                     * {@inheritdoc}
                     */
                    public function getJsonSchemaPath(): string
                    {
                        return __DIR__ . '/../fixtures/json-schemas';
                    }
                };
            }
        };
    }

    private function createStream(string $body): StreamInterface
    {
        return (new HttpFactory())->createStream($body);
    }
}
