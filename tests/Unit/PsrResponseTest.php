<?php

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Mockery as M;
use PHPUnit\Framework\Assert;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @internal
 */
final class PsrResponseTest extends TestCase
{
    public function testHasValidJson(): void
    {
        $status  = 200;
        $fixture = 'response/status_show.valid.json';

        $PsrResponse = $this->createMockedResponse($status, $fixture);

        Assert::assertTrue($PsrResponse->hasStatus($status));
        Assert::assertTrue($PsrResponse->hasJson());
        Assert::assertTrue($PsrResponse->hasValidJson('status_show.json'));
        Assert::assertEquals(json_decode($this->loadFixture($fixture)), $PsrResponse->json());
    }

    public function testHasInvalidJsonAccordingToSchema(): void
    {
        $status  = 200;
        $fixture = 'response/status_show.invalid.json';

        $PsrResponse = $this->createMockedResponse($status, $fixture);

        Assert::assertTrue($PsrResponse->hasStatus($status));
        Assert::assertTrue($PsrResponse->hasJson());
        Assert::assertFalse($PsrResponse->hasValidJson('status_show.json'));
        Assert::assertEquals(json_decode($this->loadFixture($fixture)), $PsrResponse->json());
    }

    public function testHasInvalidJson(): void
    {
        $status  = 500;
        $fixture = 'generic_error_response.txt';

        $PsrResponse = $this->createMockedResponse($status, $fixture);

        Assert::assertTrue($PsrResponse->hasStatus($status));
        Assert::assertFalse($PsrResponse->hasJson());
        Assert::assertFalse($PsrResponse->hasValidJson('status_show.json'));
        Assert::assertEquals(json_decode($this->loadFixture($fixture)), $PsrResponse->json());
    }

    public function testOriginalResponse(): void
    {
        $psrResponse  = M::mock(ResponseInterface::class);
        $PsrResponse  = $this->getPsrResponse($psrResponse);

        Assert::assertSame($PsrResponse->getPsrResponse(), $psrResponse);
    }
}
