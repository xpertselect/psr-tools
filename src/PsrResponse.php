<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PsrTools;

use Opis\JsonSchema\Validator;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

/**
 * Class PsrResponse.
 *
 * Acts as a wrapper around a `\Psr\Http\Message\ResponseInterface` providing some specific
 * utilities for interacting with the response.
 *
 * @see ResponseInterface
 */
abstract class PsrResponse
{
    /**
     * The body of the response decoded as JSON.
     */
    private mixed $json;

    /**
     * The result of the JSON decoding step.
     */
    private ?int $jsonLastError;

    /**
     * PsrResponse constructor.
     *
     * @param ResponseInterface $psrResponse The original PSR response
     */
    public function __construct(private readonly ResponseInterface $psrResponse)
    {
        $this->json          = null;
        $this->jsonLastError = null;
    }

    /**
     * The path where all json schemas can be found.
     */
    abstract public function getJsonSchemaPath(): string;

    /**
     * Retrieve the original PSR response.
     *
     * @return ResponseInterface The PSR response
     */
    final public function getPsrResponse(): ResponseInterface
    {
        return $this->psrResponse;
    }

    /**
     * Check if the status code of the response matches the given code.
     *
     * @param int $status The status to check
     *
     * @return bool Whether the response status code matches
     */
    final public function hasStatus(int $status): bool
    {
        return $this->psrResponse->getStatusCode() === $status;
    }

    /**
     * Check whether the response body can (or has been) decoded as JSON successfully.
     *
     * @return bool Whether the body contains JSON
     */
    final public function hasJson(): bool
    {
        $this->json();

        return !is_null($this->jsonLastError) && JSON_ERROR_NONE === $this->jsonLastError;
    }

    /**
     * Determines if the body of the response is conforms to a given JSON scheme.
     *
     * @param string $schemaFile The JSON scheme file to validate against
     *
     * @return bool Whether the JSON is valid
     */
    final public function hasValidJson(string $schemaFile): bool
    {
        return $this->hasJson() && $this->jsonValidatesAgainstScheme($this->json(), $schemaFile);
    }

    /**
     * Retrieve the body of the response decoded as JSON.
     *
     * @param bool $asArray Whether the response should be an array. Defaults to false
     *
     * @return mixed|array<string, mixed> The JSON decoded body
     */
    final public function json(bool $asArray = false): mixed
    {
        if (is_null($this->json)) {
            $this->json          = json_decode($this->psrResponse->getBody()->getContents());
            $this->jsonLastError = json_last_error();
        }

        return $asArray ? $this->toArray($this->json) : $this->json;
    }

    /**
     * Validates the given JSON data against a JSON scheme.
     *
     * @param mixed  $json       The JSON to validate
     * @param string $schemaFile The schema to validate against. Should only contain the filename of
     *                           the schema as it is located in the directory supplied in the
     *                           'getJsonSchemaPath' method.
     *
     * @return bool Whether the JSON is valid
     */
    final public function jsonValidatesAgainstScheme(mixed $json, string $schemaFile): bool
    {
        static $validator = null;

        if (is_null($validator)) {
            $validator = new Validator();
        }

        return $validator->validate($json, $this->getJsonSchemaContents($schemaFile))->isValid();
    }

    /**
     * Retrieve the contents of a given JSON schema from disk and return it.
     *
     * Will throw a RuntimeException if the given schema is inaccessible or if the contents of the
     * schema were unable to be retrieved.
     *
     * @param string $schemaFile The JSON schema to retrieve
     *
     * @return string The contents of the JSON schema
     */
    private function getJsonSchemaContents(string $schemaFile): string
    {
        $schemaPath = realpath($this->getJsonSchemaPath() . $schemaFile);

        if (false === $schemaPath || !is_file($schemaPath)) {
            throw new RuntimeException(sprintf(
                'Unable to access requested JSON schema file "%s"', $schemaFile
            ));
        }

        $schemaContents = file_get_contents($schemaPath);

        if (false === $schemaContents) {
            throw new RuntimeException(sprintf(
                'Unable to get contents of JSON schema file "%s"', $schemaFile
            ));
        }

        return $schemaContents;
    }

    /**
     * Cast object to array recursively.
     *
     * @param object|array<string, mixed> $object The object to cast
     *
     * @return array<string, mixed> The resulting array
     */
    private function toArray(object|array $object): array
    {
        return array_map(function($entry) {
            if (is_array($entry) || is_object($entry)) {
                return $this->toArray($entry);
            }

            return $entry;
        }, (array) $object);
    }
}
