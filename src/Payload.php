<?php

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PsrTools;

use Http\Message\MultipartStream\MultipartStreamBuilder;
use InvalidArgumentException;
use Psr\Http\Message\StreamInterface;
use RuntimeException;

/**
 * Class Payload.
 *
 * Represents a payload object with values and files. The payload class can be used to create
 * streams or objects to send with the HttpRequestService.
 */
class Payload
{
    /**
     * The MultipartStreamBuilder to create a StreamInterface.
     */
    private MultipartStreamBuilder $streamBuilder;

    /**
     * List of values of the payload.
     *
     * @var array<string, mixed>
     */
    private array $values = [];

    /**
     * List of files.
     *
     * @var array<string, array{
     *     name: string,
     *     contents: StreamInterface|resource|string,
     *     filename: null|string,
     * }> Array containing information about uploaded files.
     *   Each element in the array contains an associative
     *   array with the following keys:
     *   - "name": The name mapping to the form field.
     *   - "contents": The contents of the file (StreamInterface, resource or string)
     *   - "filename": The optional filename
     */
    private array $files = [];

    /**
     * Payload constructor.
     *
     * @param array<string, mixed> $values The values to fill the payload with. Defaults to an empty
     *                                     array
     */
    public function __construct(array $values = [])
    {
        $this->streamBuilder = new MultipartStreamBuilder();

        if (count($values) > 0) {
            $this->addValues($values);
        }
    }

    /**
     * Add an array of values to the values array in the Payload object. Uses the addValue method.
     *
     * @param array<string, mixed> $values The values to fill the payload with
     */
    public function addValues(array $values): void
    {
        foreach ($values as $name => $contents) {
            $this->addValue($name, $contents);
        }
    }

    /**
     * Add a value to the values array in the Payload object. Will overwrite if the value of the
     * name already exists. Can also overwrite files if a file with the same name exists.
     *
     * @param string $name     The name/key of the field *must* be unique
     * @param mixed  $contents The value of the field, must be an array, resource or scalar
     */
    public function addValue(string $name, mixed $contents): void
    {
        if (array_key_exists($name, $this->files)) {
            unset($this->files[$name]);
        }

        if (is_array($contents) || is_resource($contents) || is_scalar($contents)) {
            $this->values[$name] = $contents;

            return;
        }

        throw new InvalidArgumentException('Value must be an array, resource or scalar value');
    }

    /**
     * Check if the payload is currently holding any values.
     *
     * @return bool Whether the payload holds values
     */
    public function hasValues(): bool
    {
        return count($this->values) > 0;
    }

    /**
     * Add a file to the current payload. Will overwrite if there already is a file with the same
     * fieldName. Can also overwrite values if a value with the same name exists.
     *
     * @param string      $fieldName The field name, must be unique
     * @param string      $filePath  The path to the file
     * @param null|string $filename  Optional filename, will default to basename($filePath) when
     *                               missing
     */
    public function addFile(string $fieldName, string $filePath, ?string $filename = null): void
    {
        if (array_key_exists($fieldName, $this->values)) {
            unset($this->values[$fieldName]);
        }

        $fopen = fopen($filePath, 'r');

        if (false === $fopen) {
            throw new RuntimeException('Failed opening file: ' . $filePath);
        }

        $this->files[$fieldName] = [
            'name'     => $fieldName,
            'contents' => $fopen,
            'filename' => $filename ?? basename($filePath),
        ];
    }

    /**
     * Check if there are files in the current payload.
     *
     * @return bool returns true if there are files in the payload, otherwise false
     */
    public function hasFiles(): bool
    {
        return count($this->files) >= 1;
    }

    /**
     * Returns an array representation of the values stored in the object.
     *
     * @return array<string, mixed> The array representation of the values. The keys of the array
     *                              are the names of the values stored in the object, and the values
     *                              of the array are the corresponding values themselves.
     */
    public function valuesAsArray(): array
    {
        $result = [];

        foreach ($this->values as $name => $value) {
            $result[$name] = $value;
        }

        return $result;
    }

    /**
     * Returns a StreamInterface object that represents the stream containing the values and files
     * stored in the object.
     *
     * @return StreamInterface The StreamInterface object representing the stream. The stream will contain
     *                         all the values and files stored in the object.
     */
    public function getStream(): StreamInterface
    {
        $this->addValuesToStream();
        $this->addFilesToStream();

        return $this->streamBuilder->build();
    }

    /**
     * Sets the boundary for the stream builder. Should be used before the getStream method is
     * invoked.
     *
     * @param string $boundary the boundary to set for the stream builder
     */
    public function setBoundary(string $boundary): void
    {
        $this->streamBuilder->setBoundary($boundary);
    }

    /**
     * Returns the boundary value used for multipart form data.
     *
     * @return string the boundary value
     */
    public function getBoundary(): string
    {
        return $this->streamBuilder->getBoundary();
    }

    /**
     * Adds the values stored in the object to the stream builder.
     */
    private function addValuesToStream(): void
    {
        foreach ($this->values as $name => $value) {
            if (is_array($value)) {
                $this->addArrayValueToStream($name, $value);
            } else {
                $this->streamBuilder->addResource($name, $value);
            }
        }
    }

    /**
     * Adds files to the stream builder.
     */
    private function addFilesToStream(): void
    {
        foreach ($this->files as $file) {
            $options = [];

            if (!is_null($file['filename'])) {
                $options = [
                    'filename' => $file['filename'],
                ];
            }

            $this->streamBuilder->addResource($file['name'], $file['contents'], $options);
        }
    }

    /**
     * Adds an array value to the stream.
     *
     * @param string                   $name  The name of the value
     * @param array<int|string, mixed> $array The array value to be added to the stream
     */
    private function addArrayValueToStream(string $name, array $array): void
    {
        foreach ($array as $key => $value) {
            $format = sprintf('%s[%s]', $name, $key);
            if (is_array($value)) {
                $this->addArrayValueToStream($format, $value);
            } else {
                if (is_scalar($value)) {
                    $value = (string) $value;
                }
                $this->streamBuilder->addResource($format, $value);
            }
        }
    }
}
