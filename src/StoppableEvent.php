<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PsrTools;

use Psr\EventDispatcher\StoppableEventInterface;

/**
 * Class StoppableEvent.
 *
 * Base implementation for events that can stop being propagated.
 */
abstract class StoppableEvent implements StoppableEventInterface
{
    /**
     * Whether the propagation of this event is stopped.
     */
    protected bool $propagationStopped = false;

    /**
     * {@inheritdoc}
     */
    public function isPropagationStopped(): bool
    {
        return $this->propagationStopped;
    }

    /**
     * Stops propagation for this event.
     */
    public function stopPropagation(): void
    {
        $this->propagationStopped = true;
    }
}
