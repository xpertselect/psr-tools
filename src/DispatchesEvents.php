<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PsrTools;

use Psr\EventDispatcher\EventDispatcherInterface;

/**
 * Trait DispatchesEvents.
 *
 * Trait for (optionally) dispatching events.
 */
trait DispatchesEvents
{
    /**
     * The service for dispatching events.
     */
    protected ?EventDispatcherInterface $eventDispatcher = null;

    /**
     * Dispatch a given event if the current instance has a dispatcher.
     *
     * @param object $event The event to dispatch
     *
     * @return null|object The result of the dispatched event
     */
    public function dispatchEvent(object $event): ?object
    {
        if (is_null($this->eventDispatcher)) {
            return null;
        }

        return $this->eventDispatcher->dispatch($event);
    }
}
