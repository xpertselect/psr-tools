<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PsrTools\Exception;

use Exception;

/**
 * Class ClientException.
 *
 * Should be thrown when this client could not send an HTTP request.
 */
final class ClientException extends Exception
{
}
