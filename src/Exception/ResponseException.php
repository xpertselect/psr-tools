<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PsrTools\Exception;

use Exception;
use XpertSelect\PsrTools\PsrResponse;

/**
 * Class ResponseException.
 *
 * Should be thrown when the API request did not succeed for any reason.
 */
final class ResponseException extends Exception
{
    /**
     * ResponseException constructor.
     *
     * @param PsrResponse $response The response received from the API
     */
    public function __construct(public readonly PsrResponse $response)
    {
        parent::__construct();
    }
}
