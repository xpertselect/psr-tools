<?php

/**
 * This file is part of the xpertselect/psr-tools package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\PsrTools;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use SensitiveParameter;
use XpertSelect\PsrTools\Exception\ClientException;

/**
 * Class HttpRequestService.
 *
 * A service for interacting with an HTTP API using a given PSR compliant implementation.
 */
abstract class HttpRequestService
{
    /**
     * The API endpoint to communicate with.
     */
    private string $endpoint;

    /**
     * The API key for authentication and authorization.
     */
    private ?string $apiKey;

    /**
     * HttpRequestService constructor.
     *
     * @param string                  $endpoint       The API endpoint to communicate with
     * @param ClientInterface         $httpClient     The HTTP client to send HTTP requests with
     * @param RequestFactoryInterface $requestFactory The factory for creating HTTP requests
     * @param StreamFactoryInterface  $streamFactory  The factory for creating HTTP streams
     * @param string                  $authHeaderKey  The header key for authorization
     */
    public function __construct(string $endpoint, private readonly ClientInterface $httpClient,
                                private readonly RequestFactoryInterface $requestFactory,
                                private readonly StreamFactoryInterface $streamFactory,
                                private string $authHeaderKey = 'Authorization')
    {
        $this->endpoint = $this->validateEndpoint($endpoint);
        $this->apiKey   = null;
    }

    /**
     * Determine the value of the User-Agent HTTP header to include in each request.
     *
     * @return string The User-Agent value
     */
    abstract public function createUserAgent(): string;

    /**
     * Return an instance of the PsrResponse class.
     */
    abstract public function getPsrResponse(ResponseInterface $response): PsrResponse;

    /**
     * Set the Authorization header key.
     *
     * @param string $name The name of the authorization header
     */
    final public function setAuthHeaderKey(string $name): void
    {
        $this->authHeaderKey = $name;
    }

    /**
     * Sets the API key for authentication and authorization.
     *
     * @param null|string $apiKey The API key to send for authentication and authorization
     */
    final public function setApiKey(#[SensitiveParameter] ?string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Returns a boolean that indicates whether the request service has an API key set.
     *
     * @return bool The boolean that indicates whether the request service has an API key set
     */
    final public function hasApiKey(): bool
    {
        return !is_null($this->apiKey);
    }

    /**
     * Retrieve the API endpoint.
     *
     * @return string The API endpoint
     */
    final public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * Send a GET request to the given path with optional query parameters.
     *
     * @param string               $path       The path to send the GET request to
     * @param array<string, mixed> $parameters The set of query parameters
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function get(string $path = '', array $parameters = []): PsrResponse
    {
        return $this->sendRequest($this->createJsonRequest('GET', $path, parameters: $parameters));
    }

    /**
     * Send a POST request to the API.
     *
     * @param string               $path       The path of the API endpoint
     * @param Payload              $payload    Body of the request
     * @param array<string, mixed> $parameters Optional query parameters for the request
     *
     * @return PsrResponse The response received from the API
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function post(string $path, Payload $payload, array $parameters = []): PsrResponse
    {
        if ($payload->hasFiles()) {
            return $this->postStream($path, $payload->getStream(), $payload->getBoundary(), $parameters);
        }

        return $this->postJson($path, $payload->valuesAsArray(), $parameters);
    }

    /**
     * Send a POST request to the given path with optional query parameters and optional JSON data.
     *
     * @param string               $path       The path to send the POST request to
     * @param array<string, mixed> $jsonData   The JSON data to send in the body of the POST request
     * @param array<string, mixed> $parameters The set of query parameters
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function postJson(string $path = '', array $jsonData = [], array $parameters = []): PsrResponse
    {
        return $this->sendRequest($this->createJsonRequest('POST', $path, $jsonData, $parameters));
    }

    /**
     * Send a multipart/form-data POST request to the given path with optional query parameters.
     *
     * @param string               $path       The path to send the POST request to
     * @param StreamInterface      $stream     The stream to post
     * @param string               $boundary   The boundary used in the stream
     * @param array<string, mixed> $parameters The set of query parameters
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function postStream(string $path, StreamInterface $stream, string $boundary, array $parameters = []): PsrResponse
    {
        return $this->sendRequest($this->createStreamRequest('POST', $path, $stream, $boundary, $parameters));
    }

    /**
     * Send a PUT request to the API.
     *
     * @param string               $path       The API endpoint path
     * @param Payload              $payload    The data payload to be sent in the request
     * @param array<string, mixed> $parameters Additional parameters to be included in the request
     *
     * @return PsrResponse The response returned by the API
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function put(string $path, Payload $payload, array $parameters): PsrResponse
    {
        if ($payload->hasFiles()) {
            return $this->putStream($path, $payload->getStream(), $payload->getBoundary(), $parameters);
        }

        return $this->putJson($path, $parameters, $payload->valuesAsArray());
    }

    /**
     * Send a PUT request to the given path with optional query parameters and optional JSON data.
     *
     * @param string               $path       The path to send the PUT request to
     * @param array<string, mixed> $parameters The set of query parameters
     * @param array<string, mixed> $jsonData   The JSON data to send in the body of the PUT request
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function putJson(string $path = '', array $parameters = [], array $jsonData = []): PsrResponse
    {
        return $this->sendRequest($this->createJsonRequest('PUT', $path, $jsonData, $parameters));
    }

    /**
     * Send a multipart/form-data PUT request to the given path with optional query parameters.
     *
     * @param string               $path       The path to send the PUT request to
     * @param StreamInterface      $stream     The stream to put
     * @param string               $boundary   The boundary
     * @param array<string, mixed> $parameters The set of query parameters
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function putStream(string $path, StreamInterface $stream, string $boundary, array $parameters = []): PsrResponse
    {
        return $this->sendRequest($this->createStreamRequest('PUT', $path, $stream, $boundary, $parameters));
    }

    /**
     * Make a PATCH request to the specified path with the given payload and parameters.
     *
     * @param string               $path       the path where the request should be sent
     * @param Payload              $payload    the payload to send with the request
     * @param array<string, mixed> $parameters the additional parameters to include in the request
     *
     * @return PsrResponse the response received from the API
     *
     * @throws ClientException thrown when the request could not be sent
     */
    final public function patch(string $path, Payload $payload, array $parameters): PsrResponse
    {
        if ($payload->hasFiles()) {
            return $this->patchStream($path, $payload->getStream(), $payload->getBoundary(), $parameters);
        }

        return $this->patchJson($path, $parameters, $payload->valuesAsArray());
    }

    /**
     * Send a multipart/form-data PATCH request to the given path with optional query parameters.
     *
     * @param string               $path       The path to send the PATCH request to
     * @param StreamInterface      $stream     The stream to PATCH
     * @param string               $boundary   The boundary
     * @param array<string, mixed> $parameters The set of query parameters
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function patchStream(string $path, StreamInterface $stream, string $boundary, array $parameters = []): PsrResponse
    {
        return $this->sendRequest($this->createStreamRequest('PATCH', $path, $stream, $boundary, $parameters));
    }

    /**
     * Send a PATCH request to the given path with optional query parameters and optional JSON data.
     *
     * @param string               $path       The path to send the PATCH request to
     * @param array<string, mixed> $parameters The set of query parameters
     * @param array<string, mixed> $jsonData   The JSON data to send in the body of the PATCH request
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function patchJson(string $path = '', array $parameters = [], array $jsonData = []): PsrResponse
    {
        return $this->sendRequest($this->createJsonRequest('PATCH', $path, $jsonData, $parameters));
    }

    /**
     * Send a DELETE request to the given path with optional query parameters.
     *
     * @param string               $path       The path to send the PATCH request to
     * @param array<string, mixed> $parameters The set of query parameters
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function delete(string $path = '', array $parameters = []): PsrResponse
    {
        return $this->sendRequest($this->createJsonRequest('DELETE', $path, parameters: $parameters));
    }

    /**
     * Send a given request to the API.
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    private function sendRequest(RequestInterface $request): PsrResponse
    {
        try {
            return $this->getPsrResponse($this->httpClient->sendRequest($request));
        } catch (ClientExceptionInterface $e) {
            throw new ClientException('Failed to send HTTP request to the API', previous: $e);
        }
    }

    /**
     * Ensure the endpoint does not have a trailing '/'.
     *
     * @param string $endpoint The endpoint to validate
     *
     * @return string The validated endpoint
     */
    private function validateEndpoint(string $endpoint): string
    {
        return !empty($endpoint) && str_ends_with($endpoint, '/')
            ? substr($endpoint, 0, -1)
            : $endpoint;
    }

    /**
     * Creates a JSON HTTP request.
     *
     * @param string               $method     The HTTP method
     * @param string               $path       The path to send the request to
     * @param array<mixed, mixed>  $jsonData   The JSON data to send in the body of the request
     * @param array<string, mixed> $parameters The query parameters to include
     *
     * @return RequestInterface The created request
     *
     * @throws ClientException Thrown when the request could not be created
     */
    private function createJsonRequest(string $method, string $path, array $jsonData = [], array $parameters = []): RequestInterface
    {
        $uri     = $this->createUri($path, $parameters);
        $request = $this->requestFactory->createRequest($method, $uri)
            ->withHeader('Accept', 'application/json')
            ->withHeader('User-Agent', $this->createUserAgent());

        if (!empty($this->apiKey)) {
            $request = $request->withHeader($this->authHeaderKey, $this->apiKey);
        }

        if (!empty($jsonData)) {
            $jsonString = json_encode($jsonData);

            if (false === $jsonString) {
                throw new ClientException('Failed to encode json data');
            }

            $request = $request
                ->withHeader('Content-type', 'application/json')
                ->withBody($this->streamFactory->createStream($jsonString));
        }

        return $request;
    }

    /**
     *  Creates a multipart/form-data HTTP request.
     *
     * @param string               $method     The HTTP method
     * @param string               $path       The path to send the request to
     * @param StreamInterface      $stream     The streamInterface for the request
     * @param array<string, mixed> $parameters The query parameters to include
     */
    private function createStreamRequest(string $method, string $path, StreamInterface $stream, string $boundary, array $parameters = []): RequestInterface
    {
        $uri = $this->createUri($path, $parameters);

        $request = $this->requestFactory->createRequest($method, $uri)
            ->withHeader('Accept', 'application/json')
            ->withHeader('Content-Type', sprintf('multipart/form-data; boundary="%s"', $boundary))
            ->withHeader('User-Agent', $this->createUserAgent());

        if (!empty($this->apiKey)) {
            $request = $request->withHeader($this->authHeaderKey, $this->apiKey);
        }

        return $request->withBody($stream);
    }

    /**
     * Creates a URI.
     *
     * @param string               $path       The path of the URI
     * @param array<string, mixed> $parameters The query parameters of the URI
     *
     * @return string The created URI
     */
    private function createUri(string $path, array $parameters = []): string
    {
        $url = $this->endpoint . '/' . $path;

        if (count($parameters) > 0) {
            $url = $url . '?' . http_build_query(array_map(function($element) {
                if (is_bool($element)) {
                    $element = $element ? 'true' : 'false';
                }

                return $element;
            }, $parameters));
        }

        return $url;
    }
}
